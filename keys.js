/*function keys(obj) {
     Retrieve all the names of the object's properties.
     Return the keys as strings in an array.
     Based on http://underscorejs.org/#keys
}*/
function keysFnc(object={}){
    if(!object){
        return []; 
    }
    let keys=[];
    for(let key in object){
        keys.push(key);
    }
    return keys;
}
module.exports=keysFnc;