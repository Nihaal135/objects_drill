/*function mapObject(obj, cb) {
    Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    http://underscorejs.org/#mapObject
}*/
function objectMap(object, cb,key=0){
    if(!object) {
        return {};
    }
    let output = {};
    for(let key in object){
        output[key] = cb(object[key]);
    }
    return output;
}
module.exports=objectMap;

